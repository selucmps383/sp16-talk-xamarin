﻿using System;
using Xamarin.Forms;

namespace FormsSample.Forms
{
	public partial class MovieDetails : ContentPage
	{
		Movie movie;

		public MovieDetails(Movie movie)
		{
			this.movie = movie;

			InitializeComponent();
		}

		protected override void OnAppearing()
		{
			base.OnAppearing();

			imgPoster.Source = ImageSource.FromUri(new Uri(movie.poster_path));
			lblName.Text = "Title " + movie.title;
			lblReleaseDate.Text = movie.release_date;
			lblPopularity.Text = movie.popularity.ToString();
		}
	}
}