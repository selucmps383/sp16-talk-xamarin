﻿using Xamarin.Forms;
using RestSharp.Portable.HttpClient;
using RestSharp.Portable;

namespace FormsSample.Forms
{
	public partial class MoviesPage : ContentPage
	{
		private const string baseUrl = "https://api.themoviedb.org/3/";
		private const string apiUrl = "discover/movie?primary_release_date.gte=2016-02-01&primary_release_date.lte=2016-02-16&api_key=cb81f7e1fd9fbf0d1c82a03bc2011387";

		private RestClient restClient;
		private bool isInitialized;

		public MoviesPage()
		{
			InitializeComponent();

			restClient = new RestClient(baseUrl);

			listView.ItemSelected += (sender, e) =>
			{
				var selectedMovie = (Movie)e.SelectedItem;
				var movieDetailsPage = new MovieDetails(selectedMovie);
				Navigation.PushAsync(movieDetailsPage, true);	
			};

			BindingContext = this;
		}

		protected async override void OnAppearing()
		{
			base.OnAppearing();

			if(isInitialized)
			{
				return;
			}

			var request = new RestRequest(apiUrl);
			var movieResponse = await restClient.Execute<MovieResult>(request);

			// These lines are not needed if a cell template is defined in XAML. See MoviePage.xaml for the XAML template
			//var cell = new DataTemplate(typeof(ImageCell));
			//cell.SetBinding(TextCell.TextProperty, "title");
			//cell.SetBinding(TextCell.DetailProperty, new Binding("overview", stringFormat: "{0}"));
			//cell.SetBinding(ImageCell.ImageSourceProperty, "poster_path");
			//listView.ItemTemplate = cell;

			listView.ItemsSource = movieResponse.Data.results;

			isInitialized = true;
		}
	}
}